import "./App.css";

// components
import MakeMeVanish from "./components/MakeMeVanish";
import CreateABasicTimer from "./components/CreateABasicTimer";
import AddToAList from "./components/AddToAList";
import SubmitAForm from "./components/SubmitAForm";

function App() {
  return (
    <div>
      <MakeMeVanish />
      <CreateABasicTimer />
      <AddToAList />
      <SubmitAForm />
    </div>
  );
}

export default App;
