import { useState } from "react";

export default function MakeMeVanish() {
  const [isHidden, setIsHidden] = useState(false);

  function handleHideEffect() {
    setIsHidden((prevState) => !isHidden);
    return;
  }

  return isHidden === false ? (
    <>
      <div className="button1">
        <h1>1.) Make Me Vanish</h1>
        <button className="" onClick={handleHideEffect}>
          Click me!
        </button>
      </div>
    </>
  ) : (
    <div className="button1">
      <button className="" onClick={handleHideEffect}>
        Click me!
      </button>
    </div>
  );
}
